namespace Data.DataAccess
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using DataModel;

    public partial class PhoneBookContext : DbContext
    {
        public PhoneBookContext()
                : base("name=PhoneBookContext")
        {
        }

        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Person_Adress> Person_Adress { get; set; }
        public virtual DbSet<Person_Tel> Person_Tel { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>()
                .HasMany(e => e.Person_Adress)
                .WithRequired(e => e.Person)
                .HasForeignKey(e => e.id_person);

            modelBuilder.Entity<Person>()
                .HasMany(e => e.Person_Tel)
                .WithRequired(e => e.Person)
                .HasForeignKey(e => e.id_person);
        }
    }
}

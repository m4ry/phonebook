﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.DataAccess
{
    public abstract class BaseRepository<TEntity, Context> : IDisposable
       where TEntity : class//DataModel
       where Context : DbContext, new()//PhoneBookContext
    {
        Context db = new Context();
        public Context DB
        {
            get { return db; }
            set { db = value; }
        }

        public BaseRepository(Context _context)
        {
            this.DB = _context;

            this.db.Configuration.AutoDetectChangesEnabled = false;
            this.db.Configuration.ValidateOnSaveEnabled = false;
            this.db.Configuration.EnsureTransactionsForFunctionsAndCommands = true;
            this.db.Configuration.LazyLoadingEnabled = false;
        }
        public BaseRepository()
        {
            this.db.Configuration.AutoDetectChangesEnabled = false;
            this.db.Configuration.ValidateOnSaveEnabled = false;
            this.db.Configuration.EnsureTransactionsForFunctionsAndCommands = true;
            this.db.Configuration.LazyLoadingEnabled = false;
        }

        public virtual bool Add(TEntity data)
        {
            try
            {
                lock (db)
                {
                    DbEntityEntry dbEntityEntry = db.Entry(data);
                    if (dbEntityEntry.State != EntityState.Detached)
                    { dbEntityEntry.State = EntityState.Added; }

                    db.Set<TEntity>().Add(data);
                    return true;
                }
            }
            catch (Exception exp)
            {
                throw new Exception("Base Repository - Add", exp);
            }
        }

        public virtual bool AddRange(IEnumerable<TEntity> lstData)
        {
            try
            {
                lock (db)
                {
                    //DbEntityEntry dbEntityEntry = db.Entry(lstData);
                    //if (dbEntityEntry.State != EntityState.Detached)
                    //{ dbEntityEntry.State = EntityState.Added; }

                    db.Set<TEntity>().AddRange(lstData);
                    return true;
                }
            }
            catch (Exception exp)
            {
                throw new Exception("Base Repository - Add(Range)", exp);
            }
        }

        public virtual bool Remove(object id)
        {
            try
            {
                lock (db)
                { db.Set<TEntity>().Remove(db.Set<TEntity>().Find(id)); }
                return true;
            }
            catch (Exception exp)
            {
                throw new Exception("Base Repository - Add", exp);
            }
        }

        public virtual bool Remove(TEntity data)
        {
            try
            {
                lock (db)
                {
                    DbEntityEntry dbEntityEntry = db.Entry(data);
                    if (dbEntityEntry.State != EntityState.Deleted)
                    { dbEntityEntry.State = EntityState.Deleted; }

                    //db.Set<TEntity>().Attach(data);
                    db.Set<TEntity>().Remove(data);
                }
                return true;
            }
            catch (Exception exp)
            {
                throw new Exception("Base Repository - Add", exp);
            }
        }

        //public virtual bool RemoveRange(IEnumerable<TEntity> lstData)
        //{
        //    try
        //    {
        //        lock (db)
        //        {
        //            DbEntityEntry dbEntityEntry = db.Entry(lstData);
        //            if (dbEntityEntry.State != EntityState.Deleted)
        //            { dbEntityEntry.State = EntityState.Deleted; }

        //            db.Set<TEntity>().RemoveRange(lstData);
        //        }
        //        return true;
        //    }
        //    catch (Exception exp)
        //    {
        //        throw new Exception("Base Repository - Remove(Range)", exp);
        //    }
        //}

        //public virtual bool Update_Old(TEntity data)
        //{
        //    try
        //    {
        //        lock (db)
        //        { db.Set<TEntity>().AddOrUpdate(data); }
        //        return true;
        //    }
        //    catch (Exception exp)
        //    {
        //        throw new Exception("Base Repository - Update", exp);
        //    }
        //}

        public virtual bool Update(TEntity data)
        {
            try
            {
                lock (db)
                {
                    //DbEntityEntry<TEntity> dbEntityEntry = db.Entry(data);
                    db.Set<TEntity>().Attach(data);
                    db.Entry(data).State = EntityState.Modified;
                    //dbEntityEntry.State = EntityState.Modified;
                    return true;
                }
            }
            catch (Exception exp)
            {
                throw new Exception("BaseRepository Update", exp);
            }
        }

        //public virtual bool Update(TEntity data, object id)
        //{
        //    try
        //    {
        //        lock (db)
        //        {
        //            DbEntityEntry<TEntity> dbEntityEntry = db.Entry(GetById(id));
        //            db.Set<TEntity>().Attach(data);
        //            dbEntityEntry.State = EntityState.Modified;
        //            return true;
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        throw new Exception("BaseRepository Update", exp);
        //    }
        //}

        //public virtual bool Update(List<TEntity> lstData)
        //{
        //    try
        //    {
        //        foreach (var item in lstData)
        //        { if (!Update(item)) return false; }

        //        return true;
        //    }
        //    catch (Exception exp)
        //    {
        //        throw new Exception("BaseRepository Update(Range)", exp);
        //    }
        //}

        public virtual long Count()
        {
            try
            {
                lock (db)
                { return db.Set<TEntity>().Count(); }
            }
            catch (Exception exp)
            {
                throw new Exception("BaseRepository Count", exp);
            }
        }

        public virtual long Count(Expression<Func<TEntity, bool>> query)
        {
            try
            {
                lock (db)
                { return db.Set<TEntity>().Count(query); }
            }
            catch (Exception exp)
            {
                throw new Exception("BaseRepository Count", exp);
            }
        }

        public virtual bool Save()
        {
            try
            {
                int result = 0;
                lock (db)
                { result = db.SaveChanges(); }

                return (result > 0 ? true : false);
            }
            catch (Exception exp)
            {
                throw new Exception("BaseRepository Save", exp);
            }
        }

        public virtual IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> query)
        {
            try
            {
                return db.Set<TEntity>().Where(query);
            }
            catch (Exception exp)
            {
                throw new Exception("Base Repository - FindAll", exp);
            }
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            try
            {
                lock (db)
                { return db.Set<TEntity>(); }
            }
            catch (Exception exp)
            {
                throw new Exception("Base Repository - GetAll", exp);
            }
        }

        public virtual TEntity GetById(object id)
        {
            try
            {
                lock (db)
                { return db.Set<TEntity>().Find(id); }
            }
            catch (Exception exp)
            {
                throw new Exception("Base Repository - GetByID", exp);
            }
        }

        public void Dispose()
        {
            try { DB.Dispose(); }
            catch { }
        }
        ~BaseRepository()
        {
            GC.SuppressFinalize(this);
        }
    }
}

namespace Data.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Person_Adress
    {
        public int id { get; set; }

        public int id_person { get; set; }

        [Required]
        [StringLength(50)]
        public string city { get; set; }

        [Required]
        [StringLength(50)]
        public string ostan { get; set; }

        [StringLength(50)]
        public string postalCode { get; set; }

        [Required]
        [StringLength(1000)]
        public string address { get; set; }

        public virtual Person Person { get; set; }
    }
}

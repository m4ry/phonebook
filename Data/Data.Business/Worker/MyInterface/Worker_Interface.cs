﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Business.Worker.MyInterface
{
    public interface Worker_Interface<TModel>
    {
        //data
        bool Add(TModel data);
        bool Remove(TModel data);
        bool Update(TModel data);
        TModel GetById(TModel data);
        List<TModel> GetByList(Expression<Func<TModel, bool>> query, int page = 1, int count = 10);

    }
}

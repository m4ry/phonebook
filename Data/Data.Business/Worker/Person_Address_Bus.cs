﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;



namespace Data.Business.Worker
{                             
  public class Person_Address_Bus : MyInterface.Worker_Interface<Data.DataModel.Person_Adress>, IDisposable
    {
        private DataAccess.MyRepository.Person_Adress_Repo worker = new DataAccess.MyRepository.Person_Adress_Repo();
        public bool Add(DataModel.Person_Adress data)
        {
            try
            {
                worker.Add(data);
                return worker.Save();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Add", exp);
            }
        }

        public void Dispose()
        {
            try
            { worker.Dispose(); }
            catch { }
        }
        public DataModel.Person_Adress GetById(DataModel.Person_Adress data)
        {
            try
            {

                return worker.GetById(data.id);
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person GetById", exp);
            }
        }

        public List<DataModel.Person_Adress> GetByList(Expression<Func<DataModel.Person_Adress, bool>> query, int page = 1, int count = 10)
        {
            try
            {

                return worker.FindAll(query).Skip((page - 1) * count).Take(count).ToList();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person GetById", exp);
            }
        }

        public List<DataModel.Person_Adress> GetByList(int page = 1, int count = 10)
        {
            try
            {

                return worker.GetAll().Skip((page - 1) * count).Take(count).ToList();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person GetById", exp);
            }
        }
        public bool Remove(DataModel.Person_Adress data)
        {
            try
            {
                worker.Remove(data);
                return worker.Save();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Remove", exp);
            }
        }

        public bool Update(DataModel.Person_Adress data)
        {
            try
            {
                worker.Update(data);
                return worker.Save();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Update", exp);
            }
        }
    }
}



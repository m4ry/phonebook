﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.DataModel;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Linq.Expressions;

namespace Data.Business.Worker
{                             
   public  class Person_Bus : MyInterface.Worker_Interface<Data.DataModel.Person>, IDisposable
    {
       public DataAccess.MyRepository.Person_Repo worker = new DataAccess.MyRepository.Person_Repo();
        public bool Add(DataModel.Person data)
        {
            try
            {
                worker.Add(data);
                return worker.Save();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Add", exp);
            }
        }

        public void Dispose()
        {
            try
            { worker.Dispose(); }
            catch { }
        }
        public DataModel.Person GetById(DataModel.Person data)
        {
            try
            {
               
                return worker.GetById(data.id);
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person GetById", exp);
            }
        }

        public List<DataModel.Person> GetByList(Expression<Func<DataModel.Person, bool>> query, int page = 1, int count = 10)
        {
            try
            {

                return worker.FindAll(query).Skip((page - 1) * count).Take(count).ToList();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person GetById", exp);
            }
        }

        public List<DataModel.Person> GetByList(int page = 1, int count = 10)
        {
            try
            {

                return worker.GetAll().Skip((page - 1) * count).Take(count).ToList();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person GetById", exp);
            }
        }
        public bool Remove(DataModel.Person data)
        {
            try
            {
                var d = new DataModel.Person()
                {
                    codeMelli = data.codeMelli,
                    family = data.family,
                    id = data.id,
                    name = data.name
                };
                worker.Remove(d);
                return worker.Save();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Remove", exp);
            }
        }

        public bool Update(DataModel.Person data)
        {
            try
            {
                worker.Update(data);
                return worker.Save();
            }
            catch (Exception exp)
            {
                throw new Exception("Worker Person Update", exp);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PhoneBook
{ 
    public partial class FrmMain : Form
    {
        
        public int pageNumber { get { return 1; } }
        public int pageCount { get { return 10; } }
        public class show_tel
        {

            private Data.DataModel.Person_Tel MainData;
            public show_tel(Data.DataModel.Person_Tel data)
            {
                MainData = data;
            }
            public int id { get { return MainData.id; } }
            public string tel { get { return MainData.tel; } }
            public Data.Business.TelType type { get { return (Data.Business.TelType)MainData.type; } }
        }
        public FrmMain()
        {
            InitializeComponent();
            ShowData();
        }

        private void ShowData(bool reset = true)
        {
            if (reset)
                dbs_person.DataSource = new Data.Business.Worker.Person_Bus().GetByList(pageNumber, pageCount);
            else
                dbs_person.DataSource = new Data.Business.Worker.Person_Bus().GetByList(q => q != null, pageNumber, pageCount);
            //باید تابع جستجو نوشته شود q=!null به جای
            dbs_person.ResetBindings(true);
        }

        private void dbs_person_CurrentChanged(object sender, EventArgs e)
        {
            var dm_person = (Data.DataModel.Person)dbs_person.Current;
            dbs_address.DataSource = new Data.Business.Worker.Person_Address_Bus().GetByList(q => q.id_person==dm_person.id , pageNumber, pageCount);
            dbs_address.ResetBindings(true);

            dbs_tel.DataSource = new Data.Business.Worker.Person_Tel_Bus().GetByList(q => q.id_person == dm_person.id, pageNumber, pageCount);
            dbs_tel.ResetBindings(true);
        }

        private void btn_person_remove_Click(object sender, EventArgs e)
        {

            try
            {
                var dm_person = (Data.DataModel.Person)dbs_person.Current;
                new Data.Business.Worker.Person_Bus().Remove(dm_person);
                dbs_person.RemoveCurrent();
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error For Remove Data" + exp.Message);
            }
           
        }

        private void btn_person_add_Click(object sender, EventArgs e)
        {
            
            try
            {
                using (frmNewPerson frm = new frmNewPerson())
                {
                    frm.ShowDialog();
                    if (frm.MainData == null) return;
                    Data.Business.Worker.Person_Bus.Add(Data.DataModel.Person);
                    dbs_person.Add(frm.MainData);
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error For Save New" + exp.Message);
            }
        }
    }

}




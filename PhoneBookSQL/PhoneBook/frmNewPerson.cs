﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoneBook
{
    public partial class frmNewPerson : Form
    {
        private bool AddNew { get { return MainData == null; } }
        public Data.DataModel.Person MainData { get; set; }
        public frmNewPerson(Data.DataModel.Person data = null)
        {
            InitializeComponent();
            MainData = data;
            if (AddNew)
                dbs_person.DataSource = new Data.DataModel.Person();
            else
                dbs_person.DataSource = copyData(data);
            dbs_person.ResetBindings(true);

        }
        private Data.DataModel.Person copyData(Data.DataModel.Person data)
        {
            return new Data.DataModel.Person()
            {
                codeMelli=data.codeMelli,
               
            };
        }
    }

}

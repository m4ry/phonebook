﻿namespace PhoneBook
{
    partial class frmNewPerson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txt_frmperson_name = new System.Windows.Forms.TextBox();
            this.txt_frmperson_family = new System.Windows.Forms.TextBox();
            this.txt_frmperson_codeMelli = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_person_name = new System.Windows.Forms.Label();
            this.lbl_person_family = new System.Windows.Forms.Label();
            this.lbl_person_codeMelli = new System.Windows.Forms.Label();
            this.btn_frmperson_save = new System.Windows.Forms.Button();
            this.btn_frmperson_cancle = new System.Windows.Forms.Button();
            this.Address = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_frmperson_address = new System.Windows.Forms.TextBox();
            this.dbs_sddress = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_frmperson_city = new System.Windows.Forms.TextBox();
            this.txt_frmperson_ostan = new System.Windows.Forms.TextBox();
            this.txt_frmperson_postalCode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_frmperson_tel = new System.Windows.Forms.TextBox();
            this.dbs_tel = new System.Windows.Forms.BindingSource(this.components);
            this.cmb_frmperson_type = new System.Windows.Forms.ComboBox();
            this.dbs_person = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dbs_sddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbs_tel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbs_person)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_frmperson_name
            // 
            this.txt_frmperson_name.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dbs_person, "name", true));
            this.txt_frmperson_name.Location = new System.Drawing.Point(94, 33);
            this.txt_frmperson_name.Name = "txt_frmperson_name";
            this.txt_frmperson_name.Size = new System.Drawing.Size(100, 20);
            this.txt_frmperson_name.TabIndex = 0;
            // 
            // txt_frmperson_family
            // 
            this.txt_frmperson_family.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dbs_person, "family", true));
            this.txt_frmperson_family.Location = new System.Drawing.Point(94, 59);
            this.txt_frmperson_family.Name = "txt_frmperson_family";
            this.txt_frmperson_family.Size = new System.Drawing.Size(100, 20);
            this.txt_frmperson_family.TabIndex = 0;
            // 
            // txt_frmperson_codeMelli
            // 
            this.txt_frmperson_codeMelli.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dbs_person, "codeMelli", true));
            this.txt_frmperson_codeMelli.Location = new System.Drawing.Point(94, 85);
            this.txt_frmperson_codeMelli.Name = "txt_frmperson_codeMelli";
            this.txt_frmperson_codeMelli.Size = new System.Drawing.Size(100, 20);
            this.txt_frmperson_codeMelli.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(105, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // lbl_person_name
            // 
            this.lbl_person_name.AutoSize = true;
            this.lbl_person_name.Location = new System.Drawing.Point(52, 36);
            this.lbl_person_name.Name = "lbl_person_name";
            this.lbl_person_name.Size = new System.Drawing.Size(35, 13);
            this.lbl_person_name.TabIndex = 2;
            this.lbl_person_name.Text = "Name";
            // 
            // lbl_person_family
            // 
            this.lbl_person_family.AutoSize = true;
            this.lbl_person_family.Location = new System.Drawing.Point(52, 62);
            this.lbl_person_family.Name = "lbl_person_family";
            this.lbl_person_family.Size = new System.Drawing.Size(36, 13);
            this.lbl_person_family.TabIndex = 2;
            this.lbl_person_family.Text = "Family";
            // 
            // lbl_person_codeMelli
            // 
            this.lbl_person_codeMelli.AutoSize = true;
            this.lbl_person_codeMelli.Location = new System.Drawing.Point(35, 88);
            this.lbl_person_codeMelli.Name = "lbl_person_codeMelli";
            this.lbl_person_codeMelli.Size = new System.Drawing.Size(53, 13);
            this.lbl_person_codeMelli.TabIndex = 2;
            this.lbl_person_codeMelli.Text = "CodeMelli";
            // 
            // btn_frmperson_save
            // 
            this.btn_frmperson_save.Location = new System.Drawing.Point(252, 201);
            this.btn_frmperson_save.Name = "btn_frmperson_save";
            this.btn_frmperson_save.Size = new System.Drawing.Size(100, 43);
            this.btn_frmperson_save.TabIndex = 3;
            this.btn_frmperson_save.Text = "Save";
            this.btn_frmperson_save.UseVisualStyleBackColor = true;
            // 
            // btn_frmperson_cancle
            // 
            this.btn_frmperson_cancle.Location = new System.Drawing.Point(252, 250);
            this.btn_frmperson_cancle.Name = "btn_frmperson_cancle";
            this.btn_frmperson_cancle.Size = new System.Drawing.Size(100, 46);
            this.btn_frmperson_cancle.TabIndex = 4;
            this.btn_frmperson_cancle.Text = "Cancle";
            this.btn_frmperson_cancle.UseVisualStyleBackColor = true;
            // 
            // Address
            // 
            this.Address.AutoSize = true;
            this.Address.Location = new System.Drawing.Point(42, 166);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(45, 13);
            this.Address.TabIndex = 7;
            this.Address.Text = "Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(105, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 6;
            // 
            // txt_frmperson_address
            // 
            this.txt_frmperson_address.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dbs_sddress, "address", true));
            this.txt_frmperson_address.Location = new System.Drawing.Point(94, 163);
            this.txt_frmperson_address.Name = "txt_frmperson_address";
            this.txt_frmperson_address.Size = new System.Drawing.Size(434, 20);
            this.txt_frmperson_address.TabIndex = 5;
            // 
            // dbs_sddress
            // 
            this.dbs_sddress.DataSource = typeof(Data.DataModel.Person_Adress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "City";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(211, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Ostan";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(361, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "PostalCode";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(263, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 11;
            // 
            // txt_frmperson_city
            // 
            this.txt_frmperson_city.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dbs_sddress, "city", true));
            this.txt_frmperson_city.Location = new System.Drawing.Point(94, 137);
            this.txt_frmperson_city.Name = "txt_frmperson_city";
            this.txt_frmperson_city.Size = new System.Drawing.Size(100, 20);
            this.txt_frmperson_city.TabIndex = 8;
            // 
            // txt_frmperson_ostan
            // 
            this.txt_frmperson_ostan.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dbs_sddress, "ostan", true));
            this.txt_frmperson_ostan.Location = new System.Drawing.Point(252, 137);
            this.txt_frmperson_ostan.Name = "txt_frmperson_ostan";
            this.txt_frmperson_ostan.Size = new System.Drawing.Size(100, 20);
            this.txt_frmperson_ostan.TabIndex = 9;
            // 
            // txt_frmperson_postalCode
            // 
            this.txt_frmperson_postalCode.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dbs_sddress, "postalCode", true));
            this.txt_frmperson_postalCode.Location = new System.Drawing.Point(428, 137);
            this.txt_frmperson_postalCode.Name = "txt_frmperson_postalCode";
            this.txt_frmperson_postalCode.Size = new System.Drawing.Size(100, 20);
            this.txt_frmperson_postalCode.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(63, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Tel";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(105, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 16;
            // 
            // txt_frmperson_tel
            // 
            this.txt_frmperson_tel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dbs_tel, "tel", true));
            this.txt_frmperson_tel.Location = new System.Drawing.Point(213, 111);
            this.txt_frmperson_tel.Name = "txt_frmperson_tel";
            this.txt_frmperson_tel.Size = new System.Drawing.Size(100, 20);
            this.txt_frmperson_tel.TabIndex = 15;
            // 
            // dbs_tel
            // 
            this.dbs_tel.DataSource = typeof(Data.DataModel.Person_Tel);
            // 
            // cmb_frmperson_type
            // 
            this.cmb_frmperson_type.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dbs_tel, "type", true));
            this.cmb_frmperson_type.FormattingEnabled = true;
            this.cmb_frmperson_type.Items.AddRange(new object[] {
            "Mobile",
            "Tel",
            "Fax"});
            this.cmb_frmperson_type.Location = new System.Drawing.Point(93, 110);
            this.cmb_frmperson_type.Name = "cmb_frmperson_type";
            this.cmb_frmperson_type.Size = new System.Drawing.Size(101, 21);
            this.cmb_frmperson_type.TabIndex = 18;
            // 
            // dbs_person
            // 
            this.dbs_person.DataSource = typeof(Data.DataModel.Person);
            // 
            // frmNewPerson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 308);
            this.Controls.Add(this.cmb_frmperson_type);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txt_frmperson_tel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_frmperson_city);
            this.Controls.Add(this.txt_frmperson_ostan);
            this.Controls.Add(this.txt_frmperson_postalCode);
            this.Controls.Add(this.Address);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_frmperson_address);
            this.Controls.Add(this.btn_frmperson_cancle);
            this.Controls.Add(this.btn_frmperson_save);
            this.Controls.Add(this.lbl_person_codeMelli);
            this.Controls.Add(this.lbl_person_family);
            this.Controls.Add(this.lbl_person_name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_frmperson_codeMelli);
            this.Controls.Add(this.txt_frmperson_family);
            this.Controls.Add(this.txt_frmperson_name);
            this.Name = "frmNewPerson";
            this.Text = "NewPesron";
            ((System.ComponentModel.ISupportInitialize)(this.dbs_sddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbs_tel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dbs_person)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_frmperson_name;
        private System.Windows.Forms.TextBox txt_frmperson_family;
        private System.Windows.Forms.TextBox txt_frmperson_codeMelli;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_person_name;
        private System.Windows.Forms.Label lbl_person_family;
        private System.Windows.Forms.Label lbl_person_codeMelli;
        private System.Windows.Forms.Button btn_frmperson_save;
        private System.Windows.Forms.Button btn_frmperson_cancle;
        private System.Windows.Forms.Label Address;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_frmperson_address;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_frmperson_city;
        private System.Windows.Forms.TextBox txt_frmperson_ostan;
        private System.Windows.Forms.TextBox txt_frmperson_postalCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_frmperson_tel;
        private System.Windows.Forms.ComboBox cmb_frmperson_type;
        private System.Windows.Forms.BindingSource dbs_tel;
        private System.Windows.Forms.BindingSource dbs_sddress;
        private System.Windows.Forms.BindingSource dbs_person;
    }
}